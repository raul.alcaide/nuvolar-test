terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.0"
    }
  }
}
provider "google" {
  credentials = file("./nuvolar-home-test-1e59b9b63e22.json") #Replace with your own credentials
  project     = var.project
  region      = var.region
  zone        = var.zone
}