variable "project" {
  type    = string
  default = "nuvolar-home-test"
}

variable "region" {
  type    = string
  default = "europe-west1"
}

variable "zone" {
  type    = string
  default = "europe-west1-a"
}

variable "cluster_name" {
  type    = string
  default = "cluster-1"
}

variable "cluster_release_channel" {
  type    = string
  default = "REGULAR"
}

variable "gke_master_ipv4_cidr_block" {
  type    = string
  default = "10.91.128.0/28"
}

variable "artifact_registry_name" {
  type    = string
  default = "docker-repo"
}

variable "artifact_registry_description" {
  type    = string
  default = "docker repository"
}

variable "artifact_registry_format" {
  type    = string
  default = "DOCKER"
}

variable "compute_network_name" {
  type    = string
  default = "default"
}

variable "compute_network_description" {
  type    = string
  default = "Default network for the project"
}