
# Nuvolar-test

## Prerequisites
- Terraform version 4.0 or superior
- Docker CLI
- Google Cloud SDK


To change in the provider file the credentials with a json created in your own project to deploy it.
Inside it execute:

```
cd ./terraform/

terraform init
terraform plan
```

Repository
To create the Artifact Registry

```
terraform apply -f artifact_registry.tf
```

Now, the artifact_registry is created. The three images can be uploaded.
To do that, execute the following commands

```
gcloud auth configure-docker
```

and, for the three images, pull them in your local and execute:

```
docker tag IMAGE_NAME gcr.io/YOUR_PROJECT/REPOSITORY_NAME/IMAGE_NAME
docker push gcr.io/YOUR_PROJECT/REPOSITORY_NAME/IMAGE_NAME
```


## GKE cluster
To create the GKE cluster, in this case with Autopilot, execute:

```
terraform apply -f gke_autopilot.tf
terraform apply -f firewall.tf
```

## Namespace
To create the namespace, execute:

```
cd ../namespace
kubectl apply -f ./namespace.yml
```


## Services

To deploy the services, execute in every case:

## Customer

```
cd ../customer/
kubectl apply -f ./deployment.yml
kubectl apply -f ./service.yml
```

## Gateway

```
cd ../gateway/
kubectl apply -f ./deployment.yml
kubectl apply -f ./service.yml
kubectl apply -f ./ingress.yml
kubectl apply -f ./managedcertificate.yml
```

## Order

```
cd ../order/
kubectl apply -f ./deployment.yml
kubectl apply -f ./service.yml
```