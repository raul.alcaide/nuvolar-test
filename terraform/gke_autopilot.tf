resource "google_container_cluster" "gke_autopilot" {
  provider = google
  name     = var.cluster_name
  location = var.region

  private_cluster_config {
    enable_private_endpoint = false
    enable_private_nodes    = false
  }

  # Enable Autopilot for this cluster
  enable_autopilot = true

  release_channel {
    channel = var.cluster_release_channel
  }
}