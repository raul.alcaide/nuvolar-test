resource "google_compute_firewall" "firewall_http" {
  name    = "allow-http"
  network = google_compute_network.default.name

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }
}

resource "google_compute_firewall" "firewall_https" {
  name    = "allow-https"
  network = google_compute_network.default.name

  allow {
    protocol = "tcp"
    ports    = ["443"]
  }
}

resource "google_compute_network" "default" {
  name        = var.compute_network_name
  description = var.compute_network_description
}